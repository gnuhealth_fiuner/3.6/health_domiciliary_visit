from trytond.model import ModelView, ModelSQL, fields

__all__ = ['DescentreCommunitaire']

class DescentreCommunitaire(ModelView, ModelSQL):
    'My first class in Tryton!!!'
    __name__ = "descentre.communitaire"
    #Atributes
    #For the related attibrute
    du = fields.Many2One('gnuhealth.du', 'DU', help="Domiciliary Unit")
    #family_head many2one
    family_head = fields.Many2One('gnuhealth.patient', 'Family Head', required=True, help='This is the head of the family in the domiciliary unit')
    #telephone char
    telephone = fields.Char('Telephone',help='Telephone of the head family') 
    #start_date date
    start_date = fields.DateTime('Start date')
    #end_date date
    end_date = fields.DateTime('End date')
    #address function
    #TODO IT IS NOT NECESSARY?
    address = fields.Function(fields.Char('Address'),'get_address')
    #health_professional Many2One
    health_professional = fields.Many2One('gnuhealth.healthprofessional', 'Health Agent', required=True, help='Health agent in charge of the visit')
    #men_number integer
    men_number = fields.Integer('Number of men',help='Number of men living in the domiciliary unit')
    #women_number integer
    women_number = fields.Integer('Number of women',help='Number of women living in the domiciliary unit')
    #women_preg_number integer
    women_preg_number = fields.Integer('Number of pregnant women',help='Number of pregnant women living in the domiciliary unit')
    #new_born integer
    new_born = fields.Integer('Number of new borns\n (max 1 month old)',help='Number of babies between 0 to 1 month years old living in the domiciliary unit')
    #babies_number integer
    babies_number = fields.Integer('Number of babies \n (max 1 year old)',help='Number of babies between 1 month to 1 years old living in the domiciliary unit')
    #children_number5 integer
    children_number5 = fields.Integer('Number of children \n (max 5 years old)',help='Number of children between 1 to 5 years old living in the domiciliary unit')
    #children_number15 integer
    children_number15 = fields.Integer('Number of children \n (max 15 years old)',help='Number of children between 5 to 15 years old living in the domiciliary unit')
    #priority_problems text
    priority_problems = fields.Text('Priority problems',help='What are the problems we find in this domiciliarity unit?')
    #discussions text
    discussions = fields.Text('Discussions',help='Discussions in the visit')
    #material_given text
    material_given = fields.Text('Given materials',help='List of materials we give during the visit')
    #recomendations text
    recomendations = fields.Text('Given recomendations',help='List of recomendations we advise during the visit')
    #next_visit date
    next_visit = fields.Date('Next visit')
    
    
    def get_address(self, du):
        return "Address generated"
    
