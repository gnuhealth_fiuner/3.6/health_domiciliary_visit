from trytond.pool import Pool
from .descentre_communitaire import *

def register():
    Pool.register(
        DescentreCommunitaire,
        module='health_domiciliary_visit',type_='model'
        )
